﻿namespace Reflection
{
    public static class InstanceService<T>
        where T : class
    {
        public static IEnumerable<T> GetInstances()
        {
            var derivedTypes = GetDerivedTypes();

            foreach (var derivedType in derivedTypes)
            {
                var instance = Activator.CreateInstance(derivedType);

                yield return instance as T;
            }
        }

        private static IEnumerable<Type> GetDerivedTypes()
        {
            var assembly = typeof(T).Assembly;

            return assembly.GetTypes()
                .Where(t => t.IsSubclassOf(typeof(T)));
        }
    }
}
