﻿namespace ProblemSolving.Tests
{
    public class IsPalindromeTests
    {
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("      ")]
        public void ThrowExceptionIfInputIsEmpty(string input)
        {
            // Act && Assert
            Assert.Throws<ArgumentException>(() => Problems.IsPalindrome(input));
        }

        [Theory]
        [InlineData("1", true)]
        [InlineData("12", false)]
        [InlineData("11", true)]
        [InlineData("121", true)]
        [InlineData("1221", true)]
        public void IsPalindromeReturnsCorrectValue(string input, bool expected)
        {
            // Act
            var result = Problems.IsPalindrome(input);

            // Assert
            Assert.Equal(expected, result);
        }
    }
}
