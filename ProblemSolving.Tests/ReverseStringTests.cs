namespace ProblemSolving.Tests
{
    public class ReverseStringTests
    {
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("      ")]
        public void ThrowExceptionIfInputIsEmpty(string input)
        {
            // Act && Assert
            Assert.Throws<ArgumentException>(() => Problems.ReverseString(input));
        }

        [Theory]
        [InlineData("1", "1")]
        [InlineData("12", "21")]
        [InlineData("123", "321")]
        public void StringReversalIsCorrect(string input, string expected)
        {
            // Act
            var result = Problems.ReverseString(input);

            // Assert
            Assert.Equal(expected, result);
        }
    }
}
