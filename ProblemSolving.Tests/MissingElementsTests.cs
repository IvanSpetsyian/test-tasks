﻿namespace ProblemSolving.Tests
{
    public class MissingElementsTests
    {
        [Fact]
        public void ThrowExceptionIfArrayIsNull()
        {
            // Act && Assert
            Assert.Throws<ArgumentNullException>(() => Problems.MissingElements(null));
        }

        [Theory]
        [InlineData(new int[] { 4, 6, 9 }, new int[] { 5, 7, 8 })]
        [InlineData(new int[] { 2, 3, 4 }, new int[0] )]
        [InlineData(new int[] { 1, 3, 4 }, new int[] { 2 })]
        [InlineData(new int[] { 1 }, new int[0])]
        [InlineData(new int[0], new int[0])]
        public void MissingElementsReturnsCorrectValue(int[] arr, int[] expected)
        {
            // Act
            var result = Problems.MissingElements(arr);

            // Assert
            Assert.Equal(expected, result);
        }
    }
}
