﻿namespace Design
{
    public abstract class Vehicle
    {
        protected Vehicle(int maxSpeed, string manufacturer)
        {
            MaxSpeed = maxSpeed;
            Manufacturer = manufacturer;
        }

        public int MaxSpeed { get; private set; }
        public string Manufacturer { get; private set; }
    }
}
