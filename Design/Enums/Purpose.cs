﻿namespace Design.Enums
{
    internal enum Purpose : byte
    {
        City,
        Enduro,
        Sport,
    }
}
