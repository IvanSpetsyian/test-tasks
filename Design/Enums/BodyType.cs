﻿namespace Design.Enums
{
    internal enum BodyType : byte
    {
        Sedan,
        Coupe,
        SUV,
    }
}
