﻿using Design.Enums;

namespace Design
{
    internal class Motorcycle : Vehicle
    {
        public Motorcycle()
            : this(250, "Suzuki", Purpose.City)
        {
        }

        public Motorcycle(int maxSpeed, string manufacturer, Purpose purpose)
            : base(maxSpeed, manufacturer)
        {
            Purpose = purpose;
        }

        public Purpose Purpose { get; private set; }
    }
}
