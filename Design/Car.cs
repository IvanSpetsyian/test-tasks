﻿using Design.Enums;

namespace Design
{
    internal class Car : Vehicle
    {
        public Car()
            : this(150, "Toyota", BodyType.Coupe)
        {
        }

        public Car(int maxSpeed, string manufacturer, BodyType bodyType)
            : base(maxSpeed, manufacturer)
        {
            BodyType = bodyType;
        }

        public BodyType BodyType { get; private set; }
    }
}
