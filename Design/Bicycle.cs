﻿namespace Design
{
    internal class Bicycle : Vehicle
    {
        public Bicycle()
            : this(40, "Trek")
        {
        }

        public Bicycle(int maxSpeed, string manufacturer)
            : base(maxSpeed, manufacturer)
        {
        }
    }
}
