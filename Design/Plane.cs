﻿namespace Design
{
    internal class Plane : Vehicle
    {
        public Plane() : this(920, "Boeing")
        {
        }

        public Plane(int maxSpeed, string manufacturer)
            : base(maxSpeed, manufacturer)
        {
        }
    }
}
