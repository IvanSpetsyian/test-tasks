﻿using System.Text;

namespace ProblemSolving
{
    public static class Problems
    {
        public static string ReverseString(string s)
        {
            if (string.IsNullOrWhiteSpace(s))
                throw new ArgumentException("Input string cannot be null or empty.", nameof(s));

            var sb = new StringBuilder(s.Length);

            for (int i = s.Length - 1; i >= 0; i--)
            {
                sb.Append(s[i]);
            }

            return sb.ToString();
        }

        public static bool IsPalindrome(string s)
        {
            if (string.IsNullOrWhiteSpace(s))
                throw new ArgumentException("Input string cannot be null or empty.", nameof(s));

            if (s.Length == 1)
                return true;

            var middle = (s.Length / 2) - 1;

            for (int i = 0; i <= middle; i++)
            {
                if (s[i] != s[^(i + 1)])
                    return false;
            }

            return true;
        }

        public static IEnumerable<int> MissingElements(int[] arr)
        {
            if (arr is null)
                throw new ArgumentNullException(nameof(arr));

            if (arr.Length < 2)
                return Array.Empty<int>();

            var missingElements = new List<int>();

            for (int i = 0; i < arr.Length - 1; i++)
            {
                if (arr[i] + 1 != arr[i + 1])
                {
                    var missingElementsCount = arr[i + 1] - arr[i] - 1;
                    var missingRange = Enumerable.Range(arr[i] + 1, missingElementsCount);
                    missingElements.AddRange(missingRange);
                }
            }

            return missingElements;
        }
    }
}
