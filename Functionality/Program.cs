﻿using Design;
using Reflection;

Console.WriteLine(
    "Select operation." + Environment.NewLine +
    "1 - print list of vehicles," + Environment.NewLine +
    "2 - print sorted list of vehicles," + Environment.NewLine +
    "3 - input a part of name for search" + Environment.NewLine +
    "4 - write vehicle names to file" + Environment.NewLine +
    "any key - exit");

var isRunning = true;
var vehicles = InstanceService<Vehicle>.GetInstances();

do
{
    var inputKey = Console.ReadKey().Key;
    Console.WriteLine();
    switch (inputKey)
    {
        case ConsoleKey.D1:
            PrintVehicleNames(vehicles);
            break;
        case ConsoleKey.D2:
            PrintVehicleNames(SortByName(vehicles));
            break;
        case ConsoleKey.D3:
            var query = Console.ReadLine() ?? string.Empty;
            PrintVehicleNames(SearchVehicle(query, vehicles));
            break;
        case ConsoleKey.D4:
            WriteToFile(vehicles);
            Console.WriteLine("Vehicles was writted.");
            break;
        default:
            isRunning = false;
            break;
    }
} while (isRunning);

void PrintVehicleNames(IEnumerable<Vehicle> vehicles)
{
    foreach (var vehicle in vehicles)
    {
        Console.WriteLine($"\t{vehicle.GetType().Name}");
    }
}

IOrderedEnumerable<Vehicle> SortByName(IEnumerable<Vehicle> vehicles)
{
    return vehicles.OrderBy(v => v.GetType().Name);
}

IEnumerable<Vehicle> SearchVehicle(string query, IEnumerable<Vehicle> vehicles)
{
    return vehicles.Where(v => v.GetType().Name.Contains(query, StringComparison.OrdinalIgnoreCase));
}

void WriteToFile(IEnumerable<Vehicle> vehicles, string filePath = "vehicles.txt")
{
    using var sw = new StreamWriter(filePath);

    foreach (var vehicle in vehicles)
    {
        sw.WriteLine(vehicle.GetType().Name);
    }
}
